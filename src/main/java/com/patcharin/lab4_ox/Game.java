/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patcharin.lab4_ox;

import java.util.Scanner;

/**
 *
 * @author patch
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

   public void play() {
        boolean isFinish = false;
        printStart();
        newGame();
        while (!isFinish) {
            printTable();
            printTrun();
            intputRowCol();
            if (table.checkWin()) {
                printTable();
                printWiner();
                printPlayer();
                isFinish = true;
            }
            if(table.checkDraw()) {
                printTable();
                printDraw();
                printPlayer();
                isFinish = true;
            }
            table.switchPlayer(); 
            
        }
    }


   private void printStart() {
        System.out.println("Start XO Game!!!");
    }

    private void printTrun() {
        System.out.println("Turn Player: " + table.getCurrentPlayer().getSymbol());
    }

    private void intputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWiner() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!!");
    }

    private void printDraw() {
        System.out.println("Draw!!!");
    }
    private void printPlayer(){
        System.out.println(player1);
        System.out.println(player2);
    }
}

